package com.morziz.pingcalculator.liveactions;

public class LiveActions<T> {

    private T liveActionValue;
    private LiveEvent liveActionEvent;

    public LiveActions(LiveEvent liveActionEvent) {
        this.liveActionEvent = liveActionEvent;
    }
    public LiveActions(T liveActionValue, LiveEvent liveActionEvent) {
        this(liveActionEvent);
        this.liveActionValue = liveActionValue;
    }

    public T getLiveActionValue() {
        return liveActionValue;
    }

    public LiveEvent getLiveActionEvent() {
        return liveActionEvent;
    }
}
