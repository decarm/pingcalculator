package com.morziz.pingcalculator.network.manager;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.morziz.pingcalculator.network.apis.APIContract;
import com.morziz.pingcalculator.network.manager.interceptors.ConnectivityInterceptor;
import com.morziz.pingcalculator.network.utils.NetworkUtils;

import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class AppNetworkManager {
    static Cache cache;
    final static int CACHE_SIZE = 10 * 1024 * 1024; // 10 MB


    private static APIContract appClient;
    private static GsonConverterFactory gsonFactory;
    private static OkHttpClient okHttpClient;
    private static RxJava2CallAdapterFactory rxJavaFactory;

    private AppNetworkManager() {
    }

    public static void init(Context ctx) {
        cache = new Cache(ctx.getCacheDir(), CACHE_SIZE);
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        gsonFactory = GsonConverterFactory.create(gson);
        rxJavaFactory = RxJava2CallAdapterFactory.create();
        long timeOut = NetworkUtils.getTimeOut();

        okHttpClient = new OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor(new ConnectivityInterceptor(ctx))
                .addInterceptor(new HttpLoggingInterceptor())
                .connectTimeout(timeOut, TimeUnit.SECONDS)
                .readTimeout(timeOut, TimeUnit.SECONDS)
                .writeTimeout(30,TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .build();
    }

    private static OkHttpClient getOkHttpClient() {
        return okHttpClient.newBuilder()
                .build();
    }

    public static APIContract getAppClient() {
        if (appClient == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .addConverterFactory(gsonFactory)
                    .addCallAdapterFactory(rxJavaFactory)
                    .baseUrl("http://jsonplaceholder.typicode.com/")
                    .client(getOkHttpClient())
                    .build();
            appClient = retrofit.create(APIContract.class);
        }
        return appClient;
    }
}