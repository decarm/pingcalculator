package com.morziz.pingcalculator.network.repomanager;

import androidx.lifecycle.LiveData;

import com.morziz.pingcalculator.data.db.Comment;
import com.morziz.pingcalculator.data.db.Photo;
import com.morziz.pingcalculator.data.db.PingLog;
import com.morziz.pingcalculator.data.db.Post;
import com.morziz.pingcalculator.data.db.ToDo;

import java.util.List;

import io.reactivex.Observable;

public interface PingRepo {

    Observable<List<Comment>> fetchComments();

    Observable<List<Photo>> fetchPhotos();

    Observable<List<ToDo>> fetchToDos();

    Observable<List<Post>> fetchPosts();

    void updateApiStartTime(int type);

    void clearPingLogs();

    LiveData<List<PingLog>> observeAllData();
}
