package com.morziz.pingcalculator.network.manager;

import java.io.IOException;

public class NoConnectivityException extends IOException {

    private final String msg;

    public NoConnectivityException(String mContext) {
        this.msg = mContext;
    }

    @Override
    public String getMessage() {
        return msg;
    }
 
}