package com.morziz.pingcalculator.network.rx;

import io.reactivex.Scheduler;


public interface RxSchedulers {

  Scheduler androidUI();

  Scheduler io();

  Scheduler computation();

  Scheduler immediate();
}