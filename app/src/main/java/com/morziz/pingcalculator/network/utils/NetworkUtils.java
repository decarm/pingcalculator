package com.morziz.pingcalculator.network.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.security.MessageDigest;



public class NetworkUtils {
    private final static String CHECKSUM_PREFIX = "PREFH1037H";
    private final static String CHECKSUM_POSTFIX = "POSTFH1037H";
    private final static String SHA_256 = "SHA-256";
    private final static String UTF_8 = "UTF-8";

    public static final String CONNECT_TIMEOUT = "CONNECT_TIMEOUT";
    public static final String READ_TIMEOUT = "READ_TIMEOUT";
    public static final String WRITE_TIMEOUT = "WRITE_TIMEOUT";

    public static final int DEFAULT_INITIAL_DELAY = 50;
    public static final int DEFAULT_NUM_RETRIES = 2;
    public static final int DEFAULT_TIME_OUT = 10;

    public static boolean isOnline(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
        return (netInfo != null && netInfo.isConnected());
    }

    public static long getTimeOut() {
        return DEFAULT_TIME_OUT;
    }

    public static long getRetryDelay() {
        return DEFAULT_INITIAL_DELAY;
    }

    public static long getNumOfRetries() {
        return DEFAULT_NUM_RETRIES;
    }

}
