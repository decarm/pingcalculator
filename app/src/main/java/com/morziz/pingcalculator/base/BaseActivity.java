package com.morziz.pingcalculator.base;

import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;

public abstract class BaseActivity<T extends ViewDataBinding> extends AppCompatActivity {

    protected T binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, getContentView());
        initLayout(binding);
    }

    /**
     * @return Id of the layout(Resource layout) to be associated to the activity
     */
    protected abstract @LayoutRes
    int getContentView();

    /**
     * Method to introduce code in implementation of #onCreate in tmhe child activity
     */
    protected abstract void initLayout(T binding);

    public void requestFullScreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    protected void showLoadingDialog() {
        LoadingDialog.showLoadingDialog(this);
    }

    protected void cancelLoadingDialog() {
        LoadingDialog.cancelLoadingDialog();
    }

    protected void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    protected void showToast(@StringRes int stringID) {
        showToast(getString(stringID));
    }

    public T getBinding() {
        return binding;
    }

    protected void addFragment(@IdRes int fragmentContainer, Fragment fragment, String tag) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(fragmentContainer, fragment, tag)
                .commit();
    }
}