package com.morziz.pingcalculator.base;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;

public class AlertDialogManager {

    AlertDialog alertDialog;

    public void showAlertDialog(Context context, String title, String message,
                                Boolean status, String negativeButtonText) {
        if (context == null) {
            return;
        }
        alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        if (status != null)
            alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK", (dialog, which) -> {

            });

        if (!TextUtils.isEmpty(negativeButtonText)) {
            alertDialog.setButton(Dialog.BUTTON_NEGATIVE, negativeButtonText, (dialog, which) -> {
            });
        }

        alertDialog.show();

    }

    public void showAlertDialog(Context context, String title, String message,
                                Boolean status) {
        showAlertDialog(context, title, message, false, null);
    }

    public boolean checkIsShowing() {
        if (alertDialog != null) {
            if (alertDialog.isShowing()) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
}
