package com.morziz.pingcalculator.pinging;

import android.os.Handler;
import android.view.View;

import androidx.lifecycle.ViewModelProvider;

import com.morziz.pingcalculator.Constants;
import com.morziz.pingcalculator.R;
import com.morziz.pingcalculator.Utility;
import com.morziz.pingcalculator.base.BaseMVVMFragment;
import com.morziz.pingcalculator.base.ViewModelProviderFactory;
import com.morziz.pingcalculator.data.db.PingLog;
import com.morziz.pingcalculator.databinding.FragmentPingBinding;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class PingFragment extends BaseMVVMFragment<FragmentPingBinding, PingViewModel> {

    private Handler handler;

    @Override
    protected PingViewModel createViewModel() {
        return new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(PingViewModel.class);
    }

    @Override
    public int getFragmentLayout() {
        return R.layout.fragment_ping;
    }

    @Override
    public void onViewModelCreated(View view, PingViewModel viewModel) {
        handler = new Handler();
        if (isNetworkAvailable()) {
            handler.postDelayed(viewModel::fetchAll, Constants.DELAY_IN_REQUEST);
            showToast(getString(R.string.apis_hitting_soon_text));
        }
        setListeners();
    }

    private void setListeners() {
        viewModel.getIsLoading().observe(this, isLoading -> {
            if (isLoading) {
                showProgressLoader(false);
            } else {
                dismissProgressLoader();
            }
        });
        binding.timestampBtn.setOnClickListener(v -> {
            Date date = new Date();
            showAlert("Time", viewModel.getFormat().format(date) + "\nTimestamp : " + date.getTime(), getString(R.string.ok), null, null);
        });
        binding.commentBtn.setOnClickListener(v -> {
            if (isNetworkAvailable()) {
                viewModel.fetchComments();
            }});
        binding.photosBtn.setOnClickListener(v -> {
            if (isNetworkAvailable()) {
                viewModel.fetchPhotos();
            }});
        binding.todoBtn.setOnClickListener(v -> {
            if (isNetworkAvailable()) {
                viewModel.fetchToDos();
            }});
        binding.postsBtn.setOnClickListener(v -> {
            if (isNetworkAvailable()) {
                viewModel.fetchPosts();
            }});

        viewModel.observeAllLogs().observe(this, pingLogs -> {
            if (pingLogs != null) {
                for (PingLog pingLog : pingLogs) {
                    refreshUi(pingLog);
                }
            }
        });
    }

    private void refreshUi(PingLog pingLog) {
        if (pingLog != null) {
            SimpleDateFormat format = viewModel.getFormat();
            String startText = "Api Start : " + format.format(pingLog.getApiStartTimestamp());
            String endText = "Api End : " + format.format(pingLog.getApiEndTimestamp());
            ;
            String startSaveText = "Start save : " + format.format(pingLog.getSaveStartTimestamp());
            String endSaveText = "End save : " + format.format(pingLog.getSaveEndTimestamp());
            switch (pingLog.getRequestType()) {
                case Constants.REQUEST_TYPE.POSTS:
                    binding.postsLayout.tvApiStartTime.setText(startText);
                    binding.postsLayout.tvApiEndTime.setText(endText);
                    binding.postsLayout.tvSaveStartTime.setText(startSaveText);
                    binding.postsLayout.tvSaveEndTime.setText(endSaveText);
                    break;
                case Constants.REQUEST_TYPE.COMMENTS:
                    binding.commentLayout.tvApiStartTime.setText(startText);
                    binding.commentLayout.tvApiEndTime.setText(endText);
                    binding.commentLayout.tvSaveStartTime.setText(startSaveText);
                    binding.commentLayout.tvSaveEndTime.setText(endSaveText);
                    break;
                case Constants.REQUEST_TYPE.PHOTOS:
                    binding.photosLayout.tvApiStartTime.setText(startText);
                    binding.photosLayout.tvApiEndTime.setText(endText);
                    binding.photosLayout.tvSaveStartTime.setText(startSaveText);
                    binding.photosLayout.tvSaveEndTime.setText(endSaveText);
                    break;
                case Constants.REQUEST_TYPE.TODOS:
                    binding.todoLayout.tvApiStartTime.setText(startText);
                    binding.todoLayout.tvApiEndTime.setText(endText);
                    binding.todoLayout.tvSaveStartTime.setText(startSaveText);
                    binding.todoLayout.tvSaveEndTime.setText(endSaveText);
                    break;
            }
        }
    }

    private boolean isNetworkAvailable() {
        boolean networkAvailable = Utility.isNetworkAvailable(getActivity());
        if (!networkAvailable) {
            showToast(getString(R.string.no_internet));
        }
        return networkAvailable;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        handler.removeCallbacksAndMessages(null);
    }
}
