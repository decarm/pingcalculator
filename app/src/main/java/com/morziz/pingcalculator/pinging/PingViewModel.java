package com.morziz.pingcalculator.pinging;

import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.morziz.pingcalculator.Constants;
import com.morziz.pingcalculator.base.BaseViewModel;
import com.morziz.pingcalculator.data.db.PingLog;
import com.morziz.pingcalculator.liveactions.LiveActions;
import com.morziz.pingcalculator.liveactions.LiveEvent;
import com.morziz.pingcalculator.liveactions.SingleLiveEvent;
import com.morziz.pingcalculator.network.repomanager.PingRepo;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public class PingViewModel extends BaseViewModel {

    private final PingRepo mPingRepo;
    private final SingleLiveEvent<LiveActions> mLiveActions;
    private final SimpleDateFormat format;


    public PingViewModel(PingRepo pingRepo) {
        this.mPingRepo = pingRepo;
        mLiveActions = new SingleLiveEvent<LiveActions>();
        format = new SimpleDateFormat("dd-MMM-yy, HH-mm-ss.SSS", Locale.getDefault());
    }

    @Override
    public void onLoad() {
        mPingRepo.clearPingLogs();
    }

    public SingleLiveEvent<LiveActions> getLiveActions() {
        return mLiveActions;
    }

    public void fetchComments() {
        showLoader();
        AsyncTask.execute(() -> mPingRepo.updateApiStartTime(Constants.REQUEST_TYPE.COMMENTS));
        Disposable dispose = mPingRepo.fetchComments().subscribe(response -> {
            hideLoader();
            mLiveActions.setValue(new LiveActions<>(response, LiveEvent.GET_COMMENTS));
        });
        addRequestDisposable(dispose);
    }

    public void fetchPhotos() {
        showLoader();
        AsyncTask.execute(() -> mPingRepo.updateApiStartTime(Constants.REQUEST_TYPE.PHOTOS));
        Disposable dispose = mPingRepo.fetchPhotos().subscribe(response -> {
            hideLoader();
            mLiveActions.setValue(new LiveActions<>(response, LiveEvent.GET_COMMENTS));
        });
        addRequestDisposable(dispose);
    }

    public void fetchToDos() {
        showLoader();
        AsyncTask.execute(() -> mPingRepo.updateApiStartTime(Constants.REQUEST_TYPE.TODOS));
        Disposable dispose = mPingRepo.fetchToDos().subscribe(response -> {
            hideLoader();
            mLiveActions.setValue(new LiveActions<>(response, LiveEvent.GET_COMMENTS));
        });
        addRequestDisposable(dispose);
    }

    public void fetchPosts() {
        showLoader();
        AsyncTask.execute(() -> mPingRepo.updateApiStartTime(Constants.REQUEST_TYPE.POSTS));
        Disposable dispose = mPingRepo.fetchPosts().subscribe(response -> {
            hideLoader();
            mLiveActions.setValue(new LiveActions<>(response, LiveEvent.GET_COMMENTS));
        });
        addRequestDisposable(dispose);
    }

    public void fetchAll() {
        showLoader();
        AsyncTask.execute(() -> {
            mPingRepo.updateApiStartTime(Constants.REQUEST_TYPE.POSTS);
            mPingRepo.updateApiStartTime(Constants.REQUEST_TYPE.COMMENTS);
            mPingRepo.updateApiStartTime(Constants.REQUEST_TYPE.PHOTOS);
            mPingRepo.updateApiStartTime(Constants.REQUEST_TYPE.TODOS);
        });
        Disposable dispose = Observable.zip(mPingRepo.fetchComments(), mPingRepo.fetchPhotos(), mPingRepo.fetchToDos(),
                mPingRepo.fetchPosts(), (commentList, photoList, toDoList, postList) -> commentList).subscribe(commentList -> {
            hideLoader();
        });
        addRequestDisposable(dispose);
    }

    public LiveData<List<PingLog>> observeAllLogs() {
        return mPingRepo.observeAllData();
    }

    public SimpleDateFormat getFormat() {
        return format;
    }
}
