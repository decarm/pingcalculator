package com.morziz.pingcalculator.pinging;

import com.morziz.pingcalculator.R;
import com.morziz.pingcalculator.base.BaseActivity;
import com.morziz.pingcalculator.databinding.ActivityPingBinding;

public class PingActivity extends BaseActivity<ActivityPingBinding> {

    @Override
    protected int getContentView() {
        return R.layout.activity_ping;
    }

    @Override
    protected void initLayout(ActivityPingBinding binding) {
        PingFragment pingFragment = new PingFragment();
        addFragment(binding.container.getId(), pingFragment, pingFragment.getClass().getSimpleName());
    }

}