package com.morziz.pingcalculator.db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.morziz.pingcalculator.data.db.Comment;
import com.morziz.pingcalculator.data.db.Photo;
import com.morziz.pingcalculator.data.db.PingLog;

import java.util.List;

@Dao
public interface CommentsDao {

    @Insert
    void insert(Comment comment);

    @Delete
    void delete(Comment comment);

    @Update
    void update(Comment comment);

    @Query("DELETE FROM comments")
    public void deleteAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Comment> comments);

}
