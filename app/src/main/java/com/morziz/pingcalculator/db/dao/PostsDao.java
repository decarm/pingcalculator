package com.morziz.pingcalculator.db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.morziz.pingcalculator.data.db.Comment;
import com.morziz.pingcalculator.data.db.Photo;
import com.morziz.pingcalculator.data.db.Post;

import java.util.List;

@Dao
public interface PostsDao {

    @Insert
    void insert(Post post);

    @Delete
    void delete(Post post);

    @Update
    void update(Post post);

    @Query("DELETE FROM posts")
    public void deleteAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Post> posts);

}
